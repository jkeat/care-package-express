const express = require('express');
const Neon = require('@cityofzion/neon-js');
const fetch = require('fetch');

const app = express();

/**
 * Hit this endpoints to trigger dumping of new NANO transactions into NEO
 */

app.get('/store', (req, res) => {
  const props = {
    scriptHash: 'a7374f701bac9c905157bfa368eb674bc218e0a9', // Scripthash for the contract
    operation: 'storeblock', // name of operation to perform.
    args: [Neon.u.str2hexstring('string1'), Neon.u.str2hexstring('string2')] // any optional arguments to pass in. If null, use empty array.
  };

  const script = Neon.default.create.script(props)

  return Neon.rpc.Query.invokeScript(script)
    .execute('http://67ef216b.ngrok.io/')
    .then(data => {
      res.send(data);
      console.log(data) // You should get a result with state: "HALT, BREAK"
    })

  // res.send('A-OK!');
});

app.get('/get', (req, res) => {
  const props = {
    scriptHash: 'a7374f701bac9c905157bfa368eb674bc218e0a9', // Scripthash for the contract
    operation: 'retrieveblock', // name of operation to perform.
    args: [Neon.u.str2hexstring('string2')] // any optional arguments to pass in. If null, use empty array.
  };

  const script = Neon.default.create.script(props)

  return Neon.rpc.Query.invokeScript(script)
    .execute('http://67ef216b.ngrok.io/')
    .then(data => {
      res.send(data);
      console.log(data) // You should get a result with state: "HALT, BREAK"
    })

  // res.send('A-OK!');
});

app.listen(3000, () => console.log(`App listening on port 3000!`));
